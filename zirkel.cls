%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                     														  %%
%% Klasse für Korrespondenzzirkelbriefe des Mathezirkel                       %%
%%                                                                            %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{zirkel}[2022/10/18 LaTeX class]

\LoadClass[a4paper,ngerman]{scrartcl}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Pakete                                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Sprache und Schrift
\RequirePackage[ngerman]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}

% Mathematische Pakete
\RequirePackage{amsmath,amsthm,amssymb,amscd}
\RequirePackage{mathtools, stmaryrd}

% Graphiken
\RequirePackage[dvipsnames]{xcolor}
\RequirePackage{graphicx}

% Umgebungen
\RequirePackage{environ}
\RequirePackage{framed}

% Hyperlinks
\RequirePackage{url}
\RequirePackage{hyperref}

% Tabellen
\RequirePackage[shortlabels]{enumitem} % for changing the labels of enumerate using short handles, see https://tex.stackexchange.com/questions/2291/how-do-i-change-the-enumerate-list-format-to-use-letters-instead-of-the-defaul


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Einstellbare Variablen                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\schuljahr}{}
\newcommand{\klasse}{}
\newcommand{\titel}{}
\newcommand{\untertitel}{}
\newcommand{\name}{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Header                                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setlength{\unitlength}{1cm}

\newlength{\titleskip}
\setlength{\titleskip}{1.3em}
\newcommand{\makeheader}{%
    \thispagestyle{empty}%
    \begin{picture}(0,0)
        \put(0,0){%
            \vbox{%
                \noindent
                \begin{tabular}{ll}
                    Mathezirkel \hspace*{2cm} & Schuljahr \schuljahr \\
                    Universität Augsburg & Klassenstufe \klasse \\
                    \name
                \end{tabular}
                % \begin{tabular}{ll}
                %     Mathezirkel \\
                %     Universität Augsburg
                % \end{tabular}
            }
        }
        \put(13,-1.4){\includegraphics[scale=0.10]{cover}}
    \end{picture}%
    % \vspace*{1.7cm}
    \vspace*{1.0cm}
    \begin{center}
        \Large \textbf{%
            \textsf{%
                \titel \\
                \normalsize \untertitel
            }
        }
    \end{center}
    \vspace{\titleskip}
}
\newcommand{\makeheaderunlabeled}{%
    \thispagestyle{empty}%
    \begin{picture}(0,0)
        \put(0,0){%
            \vbox{%
                \noindent
                \begin{tabular}{ll}
                    Mathezirkel \hspace*{2cm} \\
                    Universität Augsburg \\
                    \name
                \end{tabular}
                % \begin{tabular}{ll}
                %     Mathezirkel \\
                %     Universität Augsburg
                % \end{tabular}
            }
        }
        \put(13,-1.4){\includegraphics[scale=0.10]{cover}}
    \end{picture}%
    % \vspace*{1.7cm}
    \vspace*{1.0cm}
    \begin{center}
        \LARGE \textbf{%
            \textsf{%
                \titel
            }
        }
    \end{center}
    \vspace*{0.48cm}
    \vspace{\titleskip}
}
\newcommand{\makepirateheader}{%
    \thispagestyle{empty}%
    \begin{picture}(0,0)
        \put(0,0){%
            \vbox{%
                \noindent 
                \begin{tabular}{ll}
                    Schatzzirkel \hspace*{2cm} & Beutezug von \schuljahr \\
                    Klabauteruni Arrrgsburg & seit \klasse ~Jahren auf hoher See \\
                    \name
                \end{tabular}
            }
        }
        \put(12.4,-2.6){\includegraphics[scale=0.19]{pirate}}
    \end{picture}%
    % \vspace*{1.7cm}
    \vspace*{1.0cm}
    \begin{center}
        \Large \textbf{%
            \textsf{%
                \titel \\
                \normalsize \untertitel
            }
        }
    \end{center}
    \vspace{\titleskip}
}
\newcommand{\makepirateheaderunlabeled}{%
    \thispagestyle{empty}%
    \begin{picture}(0,0)
        \put(0,0){%
            \vbox{%
                \noindent 
                \begin{tabular}{ll}
                    Schatzzirkel \hspace*{2cm} \\
                    Klabauteruni Arrrgsburg \\
                    \name
                \end{tabular}
            }
        }
        \put(12.4,-2.6){\includegraphics[scale=0.19]{pirate}}
    \end{picture}%
    % \vspace*{1.7cm}
    \vspace*{1.0cm}
    \begin{center}
        \Large \textbf{%
            \textsf{%
                \titel
            }
        }
    \end{center}
    \vspace*{0.48cm}
    \vspace{\titleskip}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Umgebungen                                                                 %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\definecolor{shadecolor}{rgb}{.93,.93,.93}

\theoremstyle{definition}
\newtheorem{definition}{Definition}
\newtheorem{aufg}{Aufgabe}
\newenvironment{aufgabe}[1][]
  {\begin{shaded}\vspace{-0.3cm}\begin{aufg}\emph{#1} \par\medskip}
  {\end{aufg}\vspace{-0.3cm}\end{shaded}}
\newtheorem*{loesung}{L\"osung}
\newtheorem*{hinweis}{Hinweis}
\newtheorem{beispiel}{Beispiel}
\newtheorem{bew}{Beweis}
\newenvironment{beweis}[1][]
  {\begin{shaded}\vspace{-0.3cm}\begin{bew}\emph{#1} \par\medskip}
  {\end{bew}\vspace{-0.3cm}\end{shaded}}
\theoremstyle{plain}
\newtheorem{theorem}{Satz}
\newenvironment{satz}
  {\begin{shaded}\vspace{-0.3cm}\begin{theorem}}
  {\end{theorem}\vspace{-0.3cm}\end{shaded}}
\newtheorem{beh}{Behauptung}
\newenvironment{behauptung}
  {\begin{shaded}\vspace{-0.3cm}\begin{beh}}
  {\end{beh}\vspace{-0.3cm}\end{shaded}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Layout                                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Seitenränder
\RequirePackage[left=2.5cm, right=2.5cm, top=3.5cm, bottom=4cm]{geometry}

% Einzug der ersten Zeile
\setlength{\parindent}{0cm}

% Satz
\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=10000


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Mathematische Symbole                                                      %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}

